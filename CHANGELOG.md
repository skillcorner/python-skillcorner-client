# Changelog
All notable changes to this project will be documented in this file.

## [0.5.0] - 2020-04-10

### Added
- Enable persistent http connection and use it by default for `paginated_request`

## [0.4.0] - 2020-03-26

### Changed
- Sportradar matching is now requested using a query param
- By default, `get_matches_list` requests future + past matches that were played less than 30 days ago.

## [0.3.0] - 2020-03-18

### Changed
- Reflect change to tracking endpoint in api (smaller limit and stop adding time and period)
- Handle errors (including no authorization) in paginated requests

## [0.2.0] - 2019-02-18

### Added
- Changelog

### Changed
- Change modelization of the field to have coordinates as meters centered on the center of the field
- Handle different pitch size
