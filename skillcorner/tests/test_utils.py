# -*- coding: utf8 -*-
import unittest
import json

from skillcorner.utils import get_match_unique_name


class TestUtils(unittest.TestCase):
    """
    Test utils function
    """
    def test_get_match_unique_name(self):
        """
        Test that get_match_unique_name works with space and non-ascii characters in team names
        """
        match_data = json.loads('{"date_time": "2018-05-01T18:45:00Z", "home_team": {"short_name": "Atl\u00e9tico de '
                                'Madrid"}, "away_team": {"short_name": "Be\u015fikta\u015f"}}')
        self.assertEqual(get_match_unique_name(match_data), u'20180501_Atlético de Madrid_Beşiktaş')
